var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
var request = require('request');
var server = require('../../bin/www');

describe('Testing Usuario', function(){
    beforeAll(function(done){

        mongoose.connection.close().then(() => {
        var mongoDB = 'mongodb://localhost:testdb';
            mongoose.connect(mongoDB, {useNewUrlParser: true});

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function(){
                console.log('We are connected to test database');
                done();
            });
        });
    });
    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){   //esta forma de borrar parece engorrosa se puede usar PROMISE, (es como una promesa de que borrara)
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('debe existir la reserva', (done) => {
            let usuario = new Usuario({"nombre": "Ernesto"});
            usuario.save();
            User = usuario._id;
            let bicicleta = new Bicicleta({"code": 1, "color":"rojo","modelo":"urbano"});
            bicicleta.save();
            Bici = bicicleta._id
            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){  // cuando uso POPULATE puedo hacer los .bicicleta.code y ahora uso PROMISE
                                                                                                          // .populate...populate...exec  se acumulan mediante PROMISE (asincronico)
                            console.log(reservas[0]);
                            expect(reservas.length).toBe(2);
                            expect(reservas[1].diasDeReserva()).toBe(2);
                            expect(reservas[1].bicicleta._id).toEqual(Bici);
                            expect(reservas[1].usuario._id).toEqual(User);
                            done();
                    
                    

                });
            });

        });

    });





});