// initialize the map on the "map" div with a given center and zoom
var map = L.map('main_map').setView([-34.6602762,-58.6698824] , 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'

}).addTo(map);

//L.marker([-34.6592452,-58.6749538]).addTo(map);

$.ajax({
    dataTypt:"json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.code}).addTo(map);
            
        });
    }

})