let map = L.map('main_map').setView([-25.365385,-57.453836], 14);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: (result) => {
        console.log(result);
        result.bicicletas.forEach((bici) => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
});