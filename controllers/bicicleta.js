var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function (req, res){
    Bicicleta.find({}, function(err, bicis){
    res.render('bicicletas/index', {bicis});
});
}

exports.bicicletas_create_get = function(req,res){
    res.render('bicicletas/create');
}

exports.bicicletas_create_post = function(req,res, next){
    var bici = new Bicicleta({code: req.body.id, color: req.body.color, modelo: req.body.modelo });
    bici.ubicacion = ([req.body.lat, req.body.lng]);
    bici.save(function(err){
    if (err) {
        res.render('/bicicletas/create', {errors: err.errors}); 
    }
    res.redirect('/bicicletas');
    });
}

exports.bicicletas_update_get = function(req,res){
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici});
}

exports.bicicletas_update_post = function(req,res){
    var bici = Bicicleta.findById(req.params.id);
    bici.code = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;    
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.redirect('/bicicletas');
}

exports.bicicletas_delete_post = function(req, res){
    Bicicleta.removeByCode(req.body.id,(function(err){
        res.redirect('/bicicletas');
}));
  
    
}