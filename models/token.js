const mongoose = require ('mongoose');

const Schema = mongoose.Schema;
const TokenSchema = new Schema({
    _userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Usuario'}, //mongoose se pone entre nuestro server y mongo y nos crea instancias
    token: { type: String, required: true},
    createdAt: { type: Date, required: true, default: Date.now, expires: 43200} // expira en una cantidad de tiempo.
});

module.exports = mongoose.model('Token', TokenSchema);